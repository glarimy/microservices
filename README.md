# Microservices #
Microservices are relatively smaller ploy-glot services deployed on a distributed architecture frequntly and independently, often as containers.

## Docker ##
Docker is a container technology. Docker platform include a docker-hub which is a repository of container images and docker engine to build and run containers. 

### References ###
### LAB-0 ###
Create Github and Dockerhub accounts


### LAB-1: Install Docker

```
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl status docker
sudo docker version
sudo docker info
```

### LAB-2: Get started
```
sudo docker run hello-world
sudo docker images
sudo docker ps
sudo docker ps -a
sudo docker rm <cid>
sudo docker ps -a
sudo docker images
sudo docker image rm hello-world|<iid>
sudo docker images
```

### LAB-3: Get started
```
sudo docker container run hello-world
sudo docker image ls
sudo docker container ls
sudo docker container ls -a
sudo docker container rm <cid>
sudo docker container ls -a
sudo docker image ls
sudo docker image rm hello-world|<iid>
sudo docker image ls
```

### LAB-4: Pull
```
sudo docker pull hello-world
sudo docker images
```

### LAB-5: Named containers
```
sudo docker run --name hello hello-world
sudo docker ps -a
sudo docker start hello
sudo docker ps -a
sudo docker rm hello
sudo docker ps -a
```

```
sudo docker run --name hello-1 hello-world
sudo docker run --name hello-2 hello-world
sudo docker run --name hello-3 hello-world
sudo docker ps -a
sudo docker rm hello-1 hello-2 hello-3
sudo docker ps -a
```

### LAB-6: NGINX
```
sudo docker pull nginx
sudo docker images
```

```
sudo docker run nginx
CTRL+c
sudo docker ps -a
```

```
sudo docker run -d nginx
sudo docker ps -a
sudo docker stop <cid>
sudo docker ps -a
```

```
sudo docker run -d --name nginx nginx
sudo docker stop nginx
sudo docker ps -a
```

```
sudo docker start nginx
sudo docker ps -a
sudo docker start nginx
sudo docker ps -a
sudo docker restart nginx
sudo docker ps -a
curl http://localhost:80
curl http://localhost:8080
sudo docker stop nginx
sudo docker ps -a
sudo docker rm nginx <cid> <cid>
sudo docker ps -a
```

```
sudo docker run -d -p 8080:80 --name nginx nginx
sudo docker ps -a
curl http://localhost:8080
sudo docker exec -it nginx bash
#cat /usr/share/nginx/html/index.html
#exit
sudo docker stop nginx
sudo docker rm nginx
sudo docker ps -a
```

### LAB-7: NGINX with Site 

~/some-folder/index.html

```
<html>
<head>
<title>Glarimy Server</title>
</head>
<body>
<h1>Welcome to Glarimy Server</h1>
</body>
</html>
```

```
sudo docker run -d -p 8080:80 --name nginx -v ~/some-folder:/usr/share/nginx/html nginx
curl http://localhost:8080
sudo docker exec -it nginx bash
#cat /usr/share/nginx/html/index.html
#exit
sudo docker stop nginx
sudo docker rm nginx
sudo docker image rm nginx
```

### LAB-8: Custom NGINX Container

~/some-folder/Dockerfile

```
FROM nginx:latest
COPY ./index.html /usr/share/nginx/html/index.html
```

```
cd ~/some-folder
sudo docker build -t glarimy-nginx .
sudo docker images
```

```
sudo docker run -d -p 8080:80 --name nginx glarimy-nginx
curl http://localhost:8080
sudo docker stop nginx
sudo docker rm nginx
sudo docker image rm glarimy-nginx
```

### LAB-9: Host Network
```
sudo docker run -d --network host --name nginx glarimy-nginx
curl http://localhost:80
sudo docker stop nginx
sudo docker rm nginx
```

### LAB-10: Bridge Network
```
sudo docker run -d --name nginx glarimy-nginx
sudo docker network inspect bridge
curl http://localhost:80
sudo docker -it nginx bash
#curl http://localhost:80
#curl http://nginx:80
#exit
sudo docker stop nginx
sudo docker rm nginx
```

### LAB-11: Custom Bridge Network
```
sudo docker run -d --name nginx1 --network glarimy glarimy-nginx
sudo docker run -d --name nginx2 --network glarimy glarimy-nginx
sudo docker network inspect glarimy
curl http://localhost:80
sudo docker -it nginx1 bash
#curl http://localhost:80
#curl http://nginx1:80
#curl http://nginx2:80
#exit
sudo docker stop nginx1 nginx2
sudo docker rm nginx1 nginx2
```

### LAB-12: MySQL
```
sudo docker pull mysql
sudo docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=admin mysql
sudo docker exec -it mysql bash
#mysql -u root -p
#show databases
#create database glarimy
#show databases
#quit
#exit
```

```
sudo docker restart mysql
sudo docker exec -it mysql bash
#mysql -u root -p
#show databases
#quit
#exit
sudo docker stop mysql
sudo docker rm mysql
```

```
mkdir ~/db
sudo docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=admin -v ~/db:/var/lib/mysql mysql
sudo docker exec -it mysql bash
mysql -u root -p
#show databases
#create database glarimy
#show databases
#quit
#exit
sudo docker stop mysql
sudo docker rm mysql
sudo docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=admin -v ~/db:/var/lib/mysql mysql
sudo docker exec -it mysql bash
#mysql -u root -p
#show databases
#quit
#exit
```

```
sudo docker run --name mysql-client --rm -it mysql mysql -hmysql -uroot -p
sudo docker stop mysql
sudo docker rm mysql
```

```
sudo docker network ls
sudo docker network create glarimy-nw
sudo docker network ls
```

```
sudo docker run -d --name mysql --network glarimy-nw -e MYSQL_ROOT_PASSWORD=admin -v ~/db:/var/lib/mysql mysql
sudo docker run --name mysql-client --network glarimy-nw --rm -it mysql mysql -hmysql -uroot -p
#show databases
#quit
#exit
sudo docker network rm glarimy-nw
```

### LAB-13: Volumes
```
docker volume create glarimy
docker volume ls
docker volume inspect glarimy
docker run -d --name nginx -v glarimy/app nginx
docker exec -it nginx bash
#ls
#exit
docker stop nginx
docker rm nginx
```

### LAB-14: JAVA REST-API
```
git clone https://glarimy@bitbucket.org/glarimy/glarimy-bank-service.git
docker build -t glarimy/glarimy-bank .
```

```
sudo docker login
docker push glarimy/glarimy-bank
```

```
sudo docker pull mysql
docker pull glarimy/glarimy-bank
docker network create glarimy-nw
sudo docker network ls
docker container run --name mysqldb --network glarimy-nw -e MYSQL_ROOT_PASSWORD=admin -e MYSQL_DATABASE=glarimy -d mysql
docker container logs -f mysql-db
docker container run --network glarimy-nw --name glarimy-bank -p 9090:8080 -d glarimy/glarimy-bank
docker container exec -it mysqldb bash
curl -X POST -H 'Content-Type: application/json' --data '{"name":"Krishna", "phone":9731423166}' http://localhost:8080/account
curl http://localhost:8080/account/1
sudo docker stop mysqldb glarimy-bank
sudo docker rm mysqldb glarimy-bank
sudo docker image rm mysql glarimy/glarimy-bank
```

### LAB-15: Compose
docker-compose.yml

```
version: "3"
services:
  bank:
    image: glarimy/glarimy-bank:latest
    ports:
      - "8080:8080"
    networks:
      - glarimy-nw
    depends_on:
      - mysqldb
 
  mysqldb:
    image: mysql:latest
    networks:
      - glarimy-nw
    environment:
      - MYSQL_ROOT_PASSWORD=admin
      - MYSQL_DATABASE=glarimy

networks:
  glarimy-nw: 
```

```
docker-compose up
```

```
curl -X POST -H 'Content-Type: application/json' --data '{"name":"Krishna", "phone":9731423166}' http://localhost:8080/account
curl http://localhost:8080/account/1
```

## Kubernetes ##
Kubernetes is a container orchestration tool to schedules a group of one or more containers as pods on the nodes across the cluster. Each pod is assigned an ephermal IP address known as cluster-ip using which pods on the cluster can communicate among them. Kubernetes schedules pods as per the replica set defined during the deployment and maintains the number always, by re-scheduling new pods in case of crashes. The deployments can also be exposed as service through node-port or external-ip apart from though cluster-ip.

Minikube is a tool to set up a single-node Kubernetes cluster. 

### References:###
Command Cheetsheet: `https://kubernetes.io/docs/reference/kubectl/cheatsheet/`

Online Lab: `https://www.katacoda.com/courses/kubernetes`

### Lab-0: Local Minikube Cluster ###
Setup: Ubuntu 18+ with minimum 2CPUs and 4GB RAM, Root and Sudo permissions, Docker and a web Browser

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
sudo usermod -aG docker ubuntu && newgrp docker
minikube kubectl -- get po -A
alias kubectl="minikube kubectl --"
```

### Lab-1: Hello World ###

```
minikube version
minikube start
minikube ip
kubectl --help
kubectl get nodes
kubectl get pods
kubectl get rs
kubectl get deployments
kubectl get pv
kubectl get pvc
kubectl get configmaps
kubectl get secretes
kubectl get namespaces
```

### Lab-2: Imperative Deployment ###

Deploy nginx
```
kubectl create deployment nginx --image=nginx:latest
kubectl get deployments
kubectl describe deployment <deployment-name>
kubectl get rs
kubectl describe rs <rs-name>
kubectl get pods
kubectl describe pod <pod-name>
```

Verify nginx
```
curl http://localhost:80 
ping <pod-ip>
curl http://<pod-ip>:80
kubectl logs <pod-name>
kubectl logs <pod-name> -c nginx
kubectl exec -it <pod> -- bash
#curl http://localhost:80
#curl http://<pod-ip>:80
```

Expose nginx
```
kubectl expose deployment nginx
kubectl get services
kubectl describe service nginx
curl http://<service-cluster-ip:80
```

Scale nginx
```
kubectl delete pod <pod-id>
kubectl get pods
kubectl describe pods
kubectl scale deployment/nginx --replicas=3
kubectl get rs
kubectl describe service nginx
kubectl scale deployment/nginx --replicas=3
kubectl get rs
kubectl describe service nginx
```

Remove nginx
```
kubectl delete service ngnix
kubectl delete deployment nginx
```

### Lab-3: Declarative Deployment ###

Deploy nginx 
```
vi nginx-deployment.yml
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx:latest
          ports:
            - containerPort: 80
```

```
kubectl create -f nginx-deployment.yml
kubectl get deployments
kubectl get rs
kubectl get pods
```

Expose nginx
```
vi nginx-service.yml
```

```
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
 selector:
  app: nginx
 ports:
 - port: 80
   targetPort: 80
```

```
kubectl apply -f nginx-service.yml
kubectl get services
kubectl describe service ngnix
curl http://<service-cluster-ip>:80
```

Expose from minikube ip
```
vi nginx-service.yml
```

```
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
 type: NodePort
 selector:
  app: nginx
 ports:
 - port: 80
   targetPort: 80
   nodePort: 30080
```

```
kubectl apply -f nginx-service.yml
kubectl get services
kubectl describe service ngnix
curl http://<service-cluster-ip>:80
curl http://<service-cluster-ip>:30080
curl http://<minikube-ip>:80
curl http://<minikube-ip>:30080
```

Expose from external ip

```
vi nginx-service.yml
```

```
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
 type: LoadBalancer
 selector:
  app: nginx
 ports:
 - port: 80
   targetPort: 80
   nodePort: 30080
```

```
kubectl apply -f nginx-service.yml
kubectl get services
kubectl describe service ngnix
curl http://<service-cluster-ip>:80
curl http://<service-cluster-ip>:30080
curl http://<minikube-ip>:80
curl http://<minikube-ip>:30080
curl http://<service-external-ip>:80
curl http://<service-external-ip>:30080
```

Clean up

```
kubectl delete service nginx
kubectl delete deployment nginx
```