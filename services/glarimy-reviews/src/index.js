const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 80;

const reviews = [{
    id: 1,
    rating: 4
}, {
    id: 2,
    rating: 3
}]

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/reviews', (req, res) => {
    console.log("v1 serving /reviews");
    res.json(reviews);
});

app.listen(port, () => console.log(`Review API V1 started on port ${port}!`))
