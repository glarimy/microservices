const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const request = require('request');

const app = express();
const port = process.env.PORT || 80;
const reviews = process.env.GLARIMY_REVIEWS_URL || "http://grs:80/reviews";

const products = [{
    id: 1,
    name: "laptop",
    price: 100
}, {
    id: 2,
    name: "phone",
    price: 50
}]

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/products', (req, res) => {
    console.log("vi serving /products");
    const requestOptions = {
        url: reviews,
        method: 'GET',
    };

    request(requestOptions, (err, response, body) => {
        const ratings = JSON.parse(body);
        const results = products.map(product => {
            return {
                ...product,
                rating: ratings.find(rating => product.id == rating.id)["rating"]
            }
        });

        res.json(results);
    });
});

    app.listen(port, () => console.log(`Product API V1 started on port ${port}!`))
