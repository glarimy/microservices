const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 80;

const reviews = [{
    id: 1,
    rating: "Very good"
}, {
    id: 2,
    rating: "Average"
}]

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/reviews', (req, res) => {
    console.log("v2 serving /reviews");
    res.json(reviews);
});

app.listen(port, () => console.log(`Review API V1 started on port ${port}!`))
